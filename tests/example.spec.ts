import { test, expect } from '@playwright/test';

test('has title', async ({ page, context }) => {
  await page.goto('/');

  await expect(page).toHaveTitle(/Google/);
});